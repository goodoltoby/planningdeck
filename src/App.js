import React, { Component } from 'react';
// import components
import Phase from './components/containers/Phase';
import ChasBtn from './components/ChasBtn';
// import css
import './App.css'

class App extends Component {
	/*
	*	@Constructor: Sets state when component is initialized.
	*/
  constructor(props){
    super(props);

    this.state = {
    	playing: false
    }
  }
  /*
  *	@Render:
  *		@Function: Changes this.state.playing to true if the current state is false.
  *		@Return: Switches between components depending on the state of this.state.playing.
  */	
  render() {
		const setPlaying = () => {
			if(!this.state.playing)
				this.setState({
					playing: !this.state.playing
				});
		}
    return (
      <div className="App">
      	<div className="App__wrapper">
	      	{
	      		!this.state.playing ?
	      			<ChasBtn 
	      				btnClass={ "App__btn" }
								clickHandler={ setPlaying }
								btnText={ "Start planningDeck" }							
	      			/>
			      :
	        		<Phase />
	      	}      	
      	</div>
      </div>
    );
  }
}

export default App;
