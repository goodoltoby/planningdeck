// Dispatches all data we need to set select phase
export const setSelectPhaseAction = () => dispatch => {
	dispatch({
		type: 'SET_SELECT_PHASE',
	})
	dispatch({
		type: 'FLIP_CARD',
		payload: true
	})
	dispatch({
		type: 'FAKE_PLAYERS_READY',
		payload: false
	})
	dispatch({
		type: 'PICKED_CARD',
		payload: null
	})				
}
// Dispatches all data we need to set waiting phase
export const setWaitingPhaseAction = (value) => dispatch => {
	dispatch({
		type: 'SET_WAITING_PHASE',
	})
	dispatch({
		type: 'FLIP_CARD',
		payload: false
	})
	dispatch({
		type: 'PICKED_CARD',
		payload: value
	})		
}
// Dispatches data we need to set reveal phase
export const setRevealPhaseAction = () => dispatch => {
	dispatch({
		type: 'SET_REVEAL_PHASE',
	})
}
// Dispatches data we need to set fake players
export const setFakePlayersReady = () => dispatch => {
	dispatch({
		type: 'FAKE_PLAYERS_READY',
		payload: true
	})	
}
// Dispatches data we need to set picked card
export const setPickedCardAction = (index) => dispatch => {
	dispatch({
		type: 'PICKED_CARD',
		payload: index
	})	
}
// Dispatches data to set a boolean value if card is flipped or not
export const flipCardAction = (bool) => dispatch => {
	dispatch({
		type: 'FLIP_CARD',
		payload: bool
	})
}