import React, { Component } from 'react';

class CardBack extends Component {
  /*
  * @Render: Destructuring what we need from this.props.
  */	
  render() {
  	const { cardClass, clickHandler } = this.props;
    return (  
      <figure 
        className={ "Card " + cardClass }
        onClick={ clickHandler }
      >             
				<div 
          key={ "backside" }
          className="Card__backside"
        ></div>
      </figure>
    );
  }
}

export default CardBack;
