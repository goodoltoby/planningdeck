import React, { Component } from 'react';

class CardFront extends Component {
  /*
  * @Render: Destructuring what we need from this.props.
  */  	
  render() {
  	const { cardClass, cardNumber, clickHandler } = this.props;
    return (  
      <figure 
        className={ "Card " + cardClass }
        onClick={ clickHandler }
      > 
				<div
          key={ "frontside" }
					className="Card__frontside"
				>	
					<span className="Card__frontside--upper">{ cardNumber }</span>
					<span className="Card__frontside--center">{ cardNumber }</span>
					<span className="Card__frontside--bottom">{ cardNumber }</span>						
				</div>
      </figure>
    );
  }
}

export default CardFront;
