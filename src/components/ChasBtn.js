import React, { Component } from 'react';

class ChasBtn extends Component {
  /*
  * @Render: Destructuring what we need from this.props.
  */  
  render() {
    const { btnText, btnClass, clickHandler } = this.props;
    return (
      <div 
        className={ "ChasBtn " + btnClass }
        onClick={ clickHandler }
      >
        { btnText }
      </div>
    );
  }
}

export default ChasBtn;
