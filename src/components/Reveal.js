import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
// import components
import CardFront from './CardFront';
import CardBack from './CardBack';

class Reveal extends Component {
  /*
  * @Method: Calls the revealCard method. (React Lifecycle)
  */  
  componentDidMount(){
    this.revealCard();     
  }
  /*
  * @Method: Destructuring the props we need. If cardFlipped props (global state) is false
  *   we dispatch the flipCardAction (check "./actions/phaseActions.js") which change the state to true.
  */
  revealCard(){
    const { cardFlipped } = this.props.phase,
          { flipCardAction } = this.props.phaseActions;

    if(!cardFlipped){
      flipCardAction(true);
    }
  }
  /*
  * @Method: Destructuring the props we need. If selectPhase is false we set
  *   revealPhase through the setSelectPhaseAction (check "./actions/phaseActions"). 
  *   Else we return.
  */  
  setSelectCardPhase(){
    const { selectPhase } = this.props.phase,
          { setSelectPhaseAction } = this.props.phaseActions;

    if(!selectPhase)
      setSelectPhaseAction();
    else
      return;
  }
  /*
  * @Render: Destructuring what we need from this.props.
  *   @Return: Uses the ReactCSSTransitionGroup API to handle animation between card components.
  *     Depending on the value of cardFlipped we render different components.
  *     ReactCSSTransitionGroup is used to handle classes between the two components "async"
  *     which makes it easier to add animations. (check ./styles/modules/_reveal.scss)
  */  
  render() {
  	const { pickedCard, cardFlipped } = this.props.phase;

    return (
      <section className="Reveal Playfield">
      	<div className="Reveal__infoContainer Playfield__upperSection">
          <div className="Reveal__msg">
            <span className="Reveal__msg--indicator">Press the card to select a new one.</span>
          </div>
      	</div>
      	<div className="Reveal__cardContainer Playfield__lowerSection">
          <ReactCSSTransitionGroup
            className="Reveal__flipCard"
            transitionName="flip-card"
            transitionEnterTimeout={1000}
            transitionLeaveTimeout={1000}                                        
          >          
          {
            !cardFlipped ? 
              <CardBack 
                {...this.props}
                key={ "card-back" }
                cardClass="Reveal__card"
              />
            :
              <CardFront
                {...this.props}
                key={ "card-front" }
                cardClass="Reveal__card"
                cardNumber={ pickedCard }
                clickHandler={ () => this.setSelectCardPhase() }
              />            
          }
          </ReactCSSTransitionGroup>
      	</div>
      </section>
    );
  }
}

export default Reveal;
