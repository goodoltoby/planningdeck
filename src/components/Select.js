import React, { Component } from 'react';
// import components
import CardFront from './CardFront';

class Select extends Component {
	/*
	* @Params: Takes a value which represent an int for which card we've picked.	
	* @Method: Destructuring the props we need. If we don't have a picked card
	*		we execute setWaitingPhase method with the value (picked card) as an argument. 
	*		Else we return.
	*/
	pickCard(value) {
		const { pickedCard } = this.props.phase;

		if(!pickedCard)
			this.setWaitingPhase(value);
		else
			return;
	}
	/*
	* @Params: Takes a value which represent an int for which card we've picked.
	*	@Method: Destructuring the props we need. If waitingPhase is false, we execute
	* 	the setWaitingPhaseAction (action) which demans an argument (card value). (check "./actions/phaseAction")
	*		Else we return.
	*/
	setWaitingPhase(value) {
		const { waitingPhase } = this.props.phase,
					{ setWaitingPhaseAction } = this.props.phaseActions;
		
		if(!waitingPhase)
			setWaitingPhaseAction(value);
		else
			return;		
	}
	/*
	*	@Render: Destructuring what we need from this.props.
	*		@Map: Takes the deck array (check data/cardDeck.js) and map the values that we need and use the CardFront component
	*			to render each item in the array and then return them.
	*		@Return: Calls the renderCards const which returns the cards from the map function. 
	*/
  render() {  	
  	const { deck } = this.props.data.deck;
		
    const renderCards = deck.map((card, index) =>
    	<div 
    		className="Select__cardContainer"
    		key={ index }
    	>
				<CardFront 
					{...this.props}
					cardClass={ "Select__card" }
					cardNumber={ card }
					clickHandler={ () => this.pickCard(card) }
				/>
    	</div>    	
    )

    return (
      <section className="Select Playfield">
      	<div className="Select__taskContainer Playfield__upperSection">
      		<span className="Select__task">This is the task, whats ur estimation card?</span>
      	</div>
      	<div className="Select__cards Playfield__lowerSection">
      		{ renderCards }
      	</div>      	
      </section>
    );
  }
}

export default Select;
