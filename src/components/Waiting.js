import React, { Component } from 'react';
// import components
import CardBack from './CardBack';

class Waiting extends Component {
	/*
	*	@Constructor: Sets state when component is initialized.
	*/	
  constructor(props){
    super(props);

    this.state = {
    	timer: null,
    }
  }
	/*
	*	@Method: Calls the startTimerFakePlayers method. (React Lifecycle)
	*/
	componentDidMount(){
		this.startTimerFakePlayers();
	}
	/*
	*	@Method: If we have a timer state, we clear it. (React Lifecycle)
	*/
	componentWillUnmount(){
		if(this.state.timer){
			clearTimeout(this.state.timer);
		}		
	}
	/*
	*	@Method: Destructuring the props we need. Starts a timer which calls a fake player
			action that sets fake player state to true. It also binds the timer function to the timer state
			in this component.
	*/	
	startTimerFakePlayers(){
		const { setFakePlayersReady } = this.props.phaseActions;
		
		let timer = setTimeout(() => {
			setFakePlayersReady();
		}, 3000);
		this.setState({ timer });
	}
	/*
	*	@Method: Destructuring the props we need. If revealPhase is false and fakePlayers ready we set
	*		revealPhase through the setRevealPhaseAction (check "./actions/phaseActions"). 
	*		Else we return.
	*/	
	setRevealPhase(){
		const { revealPhase, fakePlayersReady } = this.props.phase,
					{ setRevealPhaseAction } = this.props.phaseActions;

		if(!revealPhase && fakePlayersReady)
			setRevealPhaseAction();
		else
			return;
	}
	/*
	*	@Render: Destructuring what we need from this.props.
	*		Conditional check of fakePlayersReady, if its false we indicate that we're waiting for players (just fake players).
	*		If true we indicate that players are ready and makes it possible to step to the Reveal Phase.
	*		@CardBack component: We add a conditional class if fakePlayersReady is true.
	*/
  render() {
  	const { fakePlayersReady } = this.props.phase;
    return (
      <section className="Waiting Playfield">
      	<div className="Waiting__infoContainer Playfield__upperSection">
      		{
      			!fakePlayersReady ?
      				<div className="Waiting__msg">
      					<span className="Waiting__msg--notReady">Waiting for other players...</span>
      				</div>
      			:
      				<div className="Waiting__msg">
      					<span className="Waiting__msg--ready">All players are ready.</span>
								<span className="Waiting__msg--indicator">Click the card to reveal!</span>
      				</div>
      		}
      	</div>        	
      	<div className="Waiting__cardContainer Playfield__lowerSection">
	        <CardBack 
						{...this.props}
						cardClass={ "Waiting__card " + (fakePlayersReady ? 'Waiting__card--active' : null) }
						clickHandler={ () => this.setRevealPhase() }	        	
	        />    
      	</div>
      </section>
    );
  }
}

export default Waiting;
