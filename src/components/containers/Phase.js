import React, { Component } from 'react';
// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as phaseActions from '../../actions/phaseActions';
// import components
import Select from '../Select';
import Waiting from '../Waiting';
import Reveal from '../Reveal';

class Phase extends Component {
  /*
  * @Method: Executes method setPhase.
  */  
  componentWillMount(){
    this.setPhase();
  }
  /*
  * @Method:  Destructuring what we need from this.props.
  *   Check if we don't have any "phase" from global state (check "./reducers/phaseReducer"),
  *   we set (dispatch) it to "Select phase" through setSelectPhaseAction (check "./actions/phaseAction").
  */
  setPhase(){
    const { selectPhase, waitingPhase, revealPhase } = this.props.phase,
          { setSelectPhaseAction } = this.props.phaseActions;

    if (!selectPhase && !waitingPhase && !revealPhase){
      setSelectPhaseAction();
    }
  }
  /*
  * @Render: Destructuring what we need from this.props.
  *   @Return: Switches between components depending on which phase state that is active/true. 
  *   (check initialState object in ./reducers/phaseReducer)
  */
  render() {
    const { selectPhase, waitingPhase, revealPhase } = this.props.phase;
    return (
      <main className="Phase">
        {
          selectPhase ? 
            <Select 
              {...this.props}
            />
          :
            null            
        }
        {
          waitingPhase ? 
            <Waiting 
              {...this.props}
            />
          :
            null            
        }
        {
          revealPhase ? 
            <Reveal 
              {...this.props}
            />
          :
            null            
        }
      </main>
    );
  }
}

/*
  @Function: Subscribes to the store (Redux), which makes it possible to access the global states in components.
*/
const mapStateToProps = state => ({
	data: state.data,
  phase: state.phase,
})
/*
  @Function: Makes it possible to handle the global state from components through dispatch.
*/
const mapDispatchToProps = dispatch => ({
	phaseActions: bindActionCreators(phaseActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(Phase);
