import cardDeck from '../data/cardDeck';

const initialState = {
	deck: cardDeck
}

export default (state = initialState, action) => {
	switch (action.type) {
		default:
			return state
 }
}