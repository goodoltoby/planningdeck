const initialState = {	
	selectPhase: false,
	waitingPhase: false,
	revealPhase: false,
	cardFlipped: null,
	pickedCard: null,
	fakePlayersReady: false,
}

export default (state = initialState, action) => {
	switch (action.type) {	
		case 'FLIP_CARD':
			return {
				...state,
				cardFlipped: action.payload
			}
		case 'SET_SELECT_PHASE':
			return {
				...state,
				selectPhase: true,
				waitingPhase: false,
				revealPhase: false,
			}
		case 'SET_WAITING_PHASE':
			return {
				...state,
				selectPhase: false,
				waitingPhase: true,
				revealPhase: false,
			}
		case 'SET_REVEAL_PHASE':
			return {
				...state,
				selectPhase: false,
				waitingPhase: false,
				revealPhase: true,
			}
		case 'PICKED_CARD':
			return {
				...state,
				pickedCard: action.payload
			}
		case 'FAKE_PLAYERS_READY':
			return {
				...state,
				fakePlayersReady: action.payload
			}																
		default:
			return state
	}
}