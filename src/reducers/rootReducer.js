import { combineReducers } from 'redux';
import phaseReducer from './phaseReducer';
import dataReducer from './dataReducer';

export default combineReducers({
	phase: phaseReducer,
	data: dataReducer
});